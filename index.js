// [section] JSON Objects
//JSON stands for Javascript Object Notation
/*
Syntax:
{
	"propertyA": "valueA",
	"propertyB": "valueB"


}
*/
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
	
}*/

// SUB SECTION JSON ARRAYS

/*"cities": [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"}
]*/

//SECTION- JSON METHODS
	//JSON Object contains methods for parsing and converting data into stringified JSON

//SECTION Converting Data into Stringified JSON
	//stringiifed JSON is a javascript object converter into a string to be used
	//in other functions of a Javascript Application

	let batchesArr= [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];

	//the 'strinfigy' method is used to convert jAVascript objects into a string

	console.log('Result from stringify method: ');
	console.log(JSON.stringify(batchesArr));
	console.log(batchesArr);

	let data= JSON.stringify({
		name:'John',
		age: 31,
		address: {
			city:'Manila',
			country: 'Philippines'
		}
	});

	console.log(data)

//[SECTION] Using Stringify Method with Variables
	
	// When information is stored in a variable and is not hardcoded
	//into an object that is being stringifed, we can supply the value
	//with a variable


	//user details
	let firstName= prompt('What is your first name?');
	let lastName= prompt('What is your last name?');
	let age= prompt('What is your age?');
	let address= {
		city: prompt('Which City do you live in?'),
		country: prompt('Which country does your city address belong to?')
	};

	let otherData= JSON.stringify({
		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	})

	console.log(otherData);

	//[SECTION] Converting stringified JSON into Javascript objects

		//Objects are common data types used in applications because of the complex
		//data structures that can be created out of them.

	let batchesJSON = `[{ "batchName": "Batch X"}, {"batchName": "Batch Y"}]`
	console.log('Result from parse method:');
	console.log(JSON.parse(batchesJSON));

	// console.log(batchesJSON)

	let stringifiedObject = `{"name": "John", "age": "31", "address": { "city": "Manila", "country": "Philippines"} }`

		console.log(stringifiedObject);
		console.log(JSON.parse(stringifiedObject));

	